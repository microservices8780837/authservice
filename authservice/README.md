# authservice

# Sources

1. https://habr.com/ru/companies/otus/articles/681448/ - Сервер авторизации для микросервисов на Spring Boot

---

# Домашнее задание

Backend for frontends. Apigateway

# Цель:

В этом ДЗ вы научитесь добавлять в приложение аутентификацию и регистрацию пользователей.

# Описание/Пошаговая инструкция выполнения домашнего задания:

Добавить в приложение аутентификацию и регистрацию пользователей.\
Реализовать сценарий "Изменение и просмотр данных в профиле клиента".\
Пользователь регистрируется. Заходит под собой и по определенному урлу получает данные о своем профиле. \
Может поменять данные в профиле. \
Данные профиля для чтения и редактирования не должны быть доступны другим клиентам (аутентифицированным или нет).\
На выходе должны быть:

0) описание архитектурного решения и схема взаимодействия сервисов (в виде картинки)

1. команда установки приложения (из helm-а или из манифестов). Обязательно указать в каком namespace нужно
   устанавливать.\
   1*) команда установки api-gateway, если он отличен от nginx-ingress.
2. тесты постмана, которые прогоняют сценарий:

* регистрация пользователя 1
* проверка, что изменение и получение профиля пользователя недоступно без логина
* вход пользователя 1
* изменение профиля пользователя 1
* проверка, что профиль поменялся
* выход* (если есть)
* регистрация пользователя 2
* вход пользователя 2
* проверка, что пользователь2 не имеет доступа на чтение и редактирование профиля пользователя1.\
  В тестах обязательно
* наличие {{baseUrl}} для урла
* использование домена arch.homework в качестве initial значения {{baseUrl}}
* использование сгенерированных случайно данных в сценарии
* отображение данных запроса и данных ответа при запуске из командной строки с помощью newman.

---

# Solution architecture

Source from https://habr.com/ru/companies/otus/articles/681448/

![img.png](img.png)

Authentication

![img_1.png](img_1.png)

# PostgreSQL

1. `helm search hub postgresql`
2. `helm repo add bitnami https://charts.bitnami.com/bitnami` - add Bitnami repository

```text
"bitnami" has been added to your repositories 
```

3. `helm repo update` - update Helm index charts
4. `helm repo list` - get list of help repositories

```text
NAME            URL
ingress-nginx   https://kubernetes.github.io/ingress-nginx/
bitnami         https://charts.bitnami.com/bitnami

```

5. Install PostgreSQL

```
kubectl create namespace auth && helm install authservice-app-postgresql --namespace auth bitnami/postgresql --set image.tag=14.10.0-debian-11-r17,auth.username=sa,auth.password=sa,auth.database=authservice-app && kubectl apply -f ./manifests/postgres-service.yaml --namespace auth

kubectl get all -n auth

helm uninstall authservice-app-postgresql -n auth

kubectl delete namespace auth
```

---

# Push authservice to dockerhub

1. `mvn clean install -DskipTests` OR `JAVA_HOME=C:/JAVA_UTILS/openjdk-17.0.1/ mvn clean install`
2. `export DOCKER_SCAN_SUGGEST=false` - turn off docker scan
3. `docker build -t userservice .` - create Docker image from Dockerfile and marks the image with a tag
4. `docker run -it -p 8080:8000 userservice` - check docker image
5. `docker tag userservice:latest coicoin/userservice:homework3` - To tag a local image "userservice" as "
   coicoin/userservice" with the tag "homework3":
6. `docker login`
7. `docker push coicoin/userservice:homework3` - push or share a local Docker image or a repository to a central
   repository

# Helm

1. `helm create userservice-app` - создать структуру helm
2. `tree userservice-app/`

```text
wordpress/
  Chart.yaml          # A YAML file containing information about the chart
  LICENSE             # OPTIONAL: A plain text file containing the license for the chart
  README.md           # OPTIONAL: A human-readable README file
  values.yaml         # The default configuration values for this chart
  values.schema.json  # OPTIONAL: A JSON Schema for imposing a structure on the values.yaml file
  charts/             # A directory containing any charts upon which this chart depends.
  crds/               # Custom Resource Definitions
  templates/          # A directory of templates that, when combined with values,
                      # will generate valid Kubernetes manifest files.
  templates/NOTES.txt # OPTIONAL: A plain text file containing short usage notes
```

2. `helm install userservice-app ./userservice-app --dry-run` - посмотреть что будет ставиться в кластер
3. `helm install userservice-app ./userservice-app` - установить приложение helm
4. `helm uninstall userservice-app` - delete
5. `helm history userservice-app` - просмотреть историю
6. `helm upgrade userservice-app ./userservice-app` - если поменять параметр `replicaCount: 2` в values.yaml хэлма,
   можно запустить обновление.
7. `helm pull chartrepo/chartname` - download and look at the files for a published chart, without installing it

---

# ConfigMap

For the production environment, deploying secrets such as password authentication using the Kubernetes secret is highly
recommended for security.

1. `kubectl apply -f postgres-configmap.yaml`
2. `kubectl get cm`
3. `winpty kubectl exec -it userservice-dp-6797c5584b-28gm8 -- sh` - start alpine
4. `kubectl exec -it userservice-dp-6797c5584b-28gm8 -- /bin/bash` - start sh
5. `env`
6. `cat ./config/dbconfig` - open dbconfig in volume

---

# Job

1. `kubectl apply -f userservice_job.yaml`
2. `kubectl logs kuber-job-jl24k`
3. `kubectl delete job  kuber-job`
4. `kubectl get pod --watch` - watching real time pods

---

# Change default helm message using templates/NOTE.txt

```text
Installed userservice-app chart.

App listening on port {{ .Values.userserviceApp.port }}.

App will look for database at "{{- print "postgres://" .Values.userserviceApp.database.address ":" .Values.userserviceApp.database.port "/" .Values.userserviceApp.database.name -}}"

One way of creating that PostgreSQL instance is to use the bitnami/postgresql chart.

    helm repo add bitnami https://charts.bitnami.com/bitnami
    helm install postgresql bitnami/postgresql --set auth.username={{.Values.userserviceApp.database.username}},auth.password=<password>,auth.database={{.Values.userserviceApp.database.name}}
```

### Commands

1. `kubectl delete persistentvolumeclaims data-postgresql-0`
2. `helmfile sync --file k8s-demos/9-helmfile/helmfile.yaml`
3. `helmfile destroy --file k8s-demos/9-helmfile/helmfile.yaml`
4. `kc delete persistentvolumeclaims data-postgresql-0`

## Debug

1. `kubectl exec -it userservice-app-v1-5846c659b7-hjj7x sh`
2. `nslookup 10.106.199.51`

```text
Server:         10.96.0.10
Address:        10.96.0.10:53

51.199.106.10.in-addr.arpa      name = userservice-app-v1.default.svc.cluster.local
```

# Installing

1. `kubectl create namespace m && helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx/ && helm repo update && helm install nginx ingress-nginx/ingress-nginx --namespace m -f nginx-ingress.yaml`
2. `helm install userservice-app-postgresql bitnami/postgresql --set image.tag=14.10.0-debian-11-r17,auth.username=sa,auth.password=sa,auth.database=userservice-app && kubectl apply -f ./manifests/helm/userservice-app/postgres-service.yaml`
3. `helm install userservice-app ./manifests/helm/userservice-app/`
4. `helm uninstall userservice-app-postgresql`
5. `kubectl delete svc userservice-app-postgresql`
6. `helm uninstall userservice-app`
7. `kubectl delete pvc data-userservice-app-postgresql-0`

# Start

1. `http://localhost:8080/swagger-ui` OR `http://localhost:8000/swagger-ui/index.html` - swagger
2. `http://localhost:8000/user/1` - GET by id
