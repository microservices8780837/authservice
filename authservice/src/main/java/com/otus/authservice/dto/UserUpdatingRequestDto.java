package com.otus.authservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserUpdatingRequestDto {
  private String firstName;
  private String lastName;
  private String email;
  private String phone;
}
