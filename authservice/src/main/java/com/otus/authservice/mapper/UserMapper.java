package com.otus.authservice.mapper;

import com.otus.authservice.dto.UserCreationRequestDto;
import com.otus.authservice.dto.UserDto;
import com.otus.authservice.dto.UserUpdatingRequestDto;
import com.otus.authservice.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
  User userCreationDtoToUser(UserCreationRequestDto dto);

  User userUpdatingDtoToUser(UserUpdatingRequestDto dto);

  UserCreationRequestDto userToUserCreationDto(User user);

  UserUpdatingRequestDto userToUserUpdatingDto(User user);

  UserDto userToUserDto(User user);
}
