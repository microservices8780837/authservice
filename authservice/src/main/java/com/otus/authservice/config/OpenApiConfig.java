package com.otus.authservice.config;

import static org.springframework.security.config.Elements.JWT;

import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.context.annotation.Configuration;

@Configuration
@SecurityScheme(name = "bearerAuth", type = SecuritySchemeType.HTTP, scheme = "bearer",
    in = SecuritySchemeIn.HEADER, bearerFormat = JWT)
@OpenAPIDefinition(info = @Info(
    contact = @Contact(email = "homework@mail.com", name = "Main Patterns Course", url = "https://hello-world"),
    title = "springdoc-openapi",
    version = "1.0.0",
    license = @License(name = "Apache 2.0", url = "http://springdoc.org")),
    externalDocs = @ExternalDocumentation(
        description = "SpringDoc OpenApi v2.0.2 Documentation",
        url = "https://springdoc.org/v2/#Introduction"))
public class OpenApiConfig {
}
